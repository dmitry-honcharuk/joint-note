const env = require('dotenv');
const path = require('path');

env.config();

module.exports = {
  entry: ['babel-polyfill', './app/index.jsx'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      { test: /\.js|jsx$/, use: 'babel-loader' },
      { test: /\.ts|tsx?$/, loader: 'ts-loader' },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      '@modules': path.resolve(__dirname, 'app/modules'),
      '@services': path.resolve(__dirname, 'app/services'),
      '@utils': path.resolve(__dirname, 'app/utils'),
      '@consts': path.resolve(__dirname, 'app/consts'),
    },
  },
};
