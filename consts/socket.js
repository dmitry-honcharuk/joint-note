export const SOCKET_CONNECTION = {
    SUCCESS: 'socket-connection-success',
    VALIDATION_FAIL: 'socket-connection-validation-fail'
};

export const SOCKET_TOKENS_EVENTS = {
    REFRESH: 'socket-refresh-tokens',
    REFRESH_SUCCESS: 'socket-refresh-tokens-success',
    REFRESH_FAIL: 'socket-refresh-tokens-fail',
};
