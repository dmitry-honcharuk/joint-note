import { Router } from 'express';

import * as userController from '../controllers/user';

import { onlyAuthenticated } from '../middlewares/auth';

const router = Router();

// Fetch all users
router.get('/', onlyAuthenticated(), userController.getAll);

router.get('/current', onlyAuthenticated(), userController.currentUser);

// Fetch one user by id
router.get('/:id', onlyAuthenticated(), userController.getById);

// Create new user
router.post('/', onlyAuthenticated(), userController.create);

// Update user by id
router.put('/:id', onlyAuthenticated(), userController.updateById);

// Remove user by id
router.delete('/:id', onlyAuthenticated(), userController.removeById);

export default router;
