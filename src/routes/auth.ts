import { Router } from 'express';

import * as authController from '../controllers/auth';
import { withToken } from '../middlewares/auth';

const router = Router();

router.post('/register', authController.registerUser);

router.post('/login', authController.loginUser);

router.get('/refresh', withToken, authController.refresh);

export default router;
