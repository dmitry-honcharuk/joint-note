import { Router } from 'express';

import * as notesController from '../controllers/notes';
import { onlyAuthenticated } from '../middlewares/auth';

const router = Router();

router.post('/', onlyAuthenticated(), notesController.createNote);

router.get('/', onlyAuthenticated(), notesController.getNotes);

router.get('/shared', onlyAuthenticated(), notesController.getSharedNotes);

router.delete('/shared/:id', onlyAuthenticated(), notesController.stopCollaborating);

router.get('/:id', onlyAuthenticated(), notesController.getNote);

router.get(
  '/:id/collaborators/search',
  onlyAuthenticated(),
  notesController.searchCollaboratorPretendersByEmail,
);

router.get(
  '/:id/collaborators',
  onlyAuthenticated(),
  notesController.getCollaborators,
);

router.post(
  '/:id/collaborators',
  onlyAuthenticated(),
  notesController.addCollaborators,
);

router.delete(
  '/:id/collaborators/:collaboratorId',
  onlyAuthenticated(),
  notesController.deleteCollaborator,
);

router.delete('/:id', onlyAuthenticated(), notesController.deleteNote);

router.post('/:id/items', onlyAuthenticated(), notesController.addItems);

export default router;
