import { Router } from 'express';

import authRoutes from './auth';
import userRoutes from './users';
import noteRoutes from './notes';
import itemRoutes from './items';

const router = Router();

router.use('/auth', authRoutes);
router.use('/users', userRoutes);
router.use('/notes', noteRoutes);
router.use('/items', itemRoutes);

export default router;
