import { Router } from 'express';

import * as itemsController from '../controllers/items';
import { onlyAuthenticated } from '../middlewares/auth';

const router = Router();

router.get('/:id/complete', onlyAuthenticated(), itemsController.completeItem);

router.get('/:id/undo', onlyAuthenticated(), itemsController.undoItem);

export default router;
