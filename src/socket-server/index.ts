import { Server } from 'http';

import SocketService from '../services/socket-service';
import { SOCKET_CONNECTION } from '../../consts/socket';

export function initSocketServer(server: Server) {
  const socketService = new SocketService(server);

  socketService.onConnect(socket => {
    console.log('user connected');

    socket.emit('action', {
      data: 'superdata'
    });

    socket.subscribe('disconnect', () => {
      console.log('user disconnected');
    });
  });
}
