import { Request, Response } from 'express';

import * as itemService from '../services/item-service';

export const completeItem = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    await itemService.completeItem(id);

    return res.json();
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const undoItem = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    await itemService.undoItem(id);

    return res.json();
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};
