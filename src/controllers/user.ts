import { Request, Response } from 'express';

import * as userService from '../services/user-service';

export const getAll = async (req: Request, res: Response) => {
  try {
    const users = await userService.gatAll();
    return res.json(users);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const getById = async (req: Request, res: Response) => {
  const { id } = req.params;
  const user = await userService.getById(id);

  if (user) {
    return res.json(user.toJSON());
  } else {
    return res.send('no user');
  }
};

export const create = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;
    const user = await userService.create(email, password);
    return res.json(user.toJSON());
  } catch ({ message }) {
    return res.status(400).send(message);
  }
};

export const updateById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const fields = req.body;

    await userService.updateById(id, fields);

    return res.send(null);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const removeById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    await userService.deleteById(id);

    return res.send(null);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const currentUser = async (req: Request, res: Response) => {
  try {
    const {
      user: {
        id,
        email,
      }
    } = req;

    return res.json({ id, email });
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};
