import { Request, Response } from 'express';
import * as Bluebird from 'bluebird';

import * as itemService from '../services/item-service';
import * as collaboratorService from '../services/collaborators-service';
import * as userService from '../services/user-service';
import * as noteService from '../services/note-service';

export const createNote = async (req: Request, res: Response) => {
  try {
    const {
      user,
      body: { name, items },
    } = req;

    const note = await noteService.createUserNote(+user.id, name);

    if (items && items.length !== 0) {
      await itemService.createItems(items, note.id);
    }

    return res.json(note);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const getNotes = async (req: Request, res: Response) => {
  const { user } = req;

  try {
    const notes = await noteService.getUserNotes(user.id);
    return res.json(notes);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const getSharedNotes = async (req: Request, res: Response) => {
  const { user } = req;

  try {
    const sharedNotes = await noteService.getSharedNotes(user.id);

    return res.json(sharedNotes);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
};

export const stopCollaborating = async (req: Request, res: Response) => {
  const {
    user: { id: userID },
    params: { id: noteId },
  } = req;

  try {
    await collaboratorService.removeCollaborator(noteId, userID);

    return res.json();
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
};

export const deleteNote = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { id: userId } = req.user;
  const note = await noteService.getNoteById({ noteId: id, attributes: ['CreatorId'] });

  try {
    if (userId === note.CreatorId) {
      const isDeleted = await noteService.deleteNoteById(id);
      if (!isDeleted) {
        return res.status(400).json({ error: 'Not deleted' });
      }
      return res.json();
    } else {
      return res.status(403).json({ error: 'You can\'t delete this note' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const getNote = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { id: userId } = req.user;

  try {
    const note = await noteService.getNoteById({
      noteId: id,
      attributes: ['id', 'name', 'CreatorId'],
      includeCreator: true,
      creatorAttributes: ['email'],
    });

    const [noteItems, collaborators] = await Bluebird.all([
      itemService.getNoteItems(note.id, [
        'id',
        'title',
        'completed',
      ]),
      collaboratorService.getNoteCollaborators({
        noteId: note.id,
        attributes: ['id', 'UserId'],
        includeUserInfo: true,
        userAttributes: ['id', 'email'],
      }),
    ]);

    const collaboratorsIds = collaborators.map(({ UserId }) => UserId);
    const isUserCollaborator = collaboratorsIds.includes(userId);

    if (userId === note.CreatorId || isUserCollaborator) {
      return res.json({
        note,
        collaborators,
        items: noteItems,
      });
    } else {
      return res.status(403).json({ error: 'Forbidden' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const getCollaborators = async (req: Request, res: Response) => {
  const { id: noteId }: { id: string } = req.params;

  try {
    const collaborators = await collaboratorService.getNoteCollaborators({
      noteId: +noteId,
      attributes: ['id', 'UserId'],
      includeUserInfo: true,
      userAttributes: ['id', 'email'],
    });

    return res.json(collaborators);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const addCollaborators = async (req: Request, res: Response) => {
  const { id: noteId }: { id: string } = req.params;
  const { id: userId }: { id?: number } = req.user;
  const { collaborators } = req.body;

  try {
    const note = await noteService.getNoteById({
      noteId: +noteId,
      attributes: ['CreatorId'],
    });

    if (userId === note.CreatorId) {
      await collaboratorService.createCollaborators(
        collaborators.map(id => ({
          userId: id,
          noteId: +noteId,
        })),
      );
      return res.json();
    } else {
      return res.status(403).json({ error: 'Forbidden' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const searchCollaboratorPretendersByEmail = async (
  req: Request,
  res: Response,
) => {
  try {
    const { q } = req.query;
    const { id: noteId }: { id: string } = req.params;
    const { id: userId }: { id?: number } = req.user;

    const existingCollaborators = await collaboratorService.getNoteCollaborators(
      {
        noteId: +noteId,
        attributes: ['UserId'],
      },
    );
    const existingCollaboratorsIds = existingCollaborators.map(
      ({ UserId }) => UserId,
    );

    const users = await userService.searchByEmail(
      q,
      ['id', 'email'],
      [userId, ...existingCollaboratorsIds],
    );

    return res.send(users);
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const addItems = async (req: Request, res: Response) => {
  const {
    params: { id: noteId },
    body: { items: itemNames },
    user: { id: userId },
  } = req;

  try {
    const isUserCreator = await noteService.isUserCreator(+noteId, userId);
    const isUserCollaborator = await collaboratorService.isUserCollaborator(
      +noteId,
      userId,
    );

    if (isUserCreator || isUserCollaborator) {
      const items = await itemService.createItems(itemNames, noteId);

      return res.json(
        items.map(item => ({
          id: item.id,
          title: item.title,
          completed: item.completed,
        })),
      );
    } else {
      return res.status(403).json({ error: 'Forbidden' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const deleteCollaborator = async (req: Request, res: Response) => {
  try {
    const {
      params: { collaboratorId, id: noteId },

      user: { id: userId },
    } = req;

    const note = await noteService.getNoteById({
      noteId: +noteId,
      attributes: ['CreatorId'],
    });

    if (userId === note.CreatorId) {
      await collaboratorService.removeCollaborator(noteId, collaboratorId);

      return res.json();
    } else {
      return res.status(403).json({ error: 'Forbidden' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};
