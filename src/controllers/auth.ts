import { Request, Response } from 'express';

import authService from '../services/auth-service';
import * as userService from '../services/user-service';

export const registerUser = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;

    if (!email) {
      return res.status(401).json({ email: 'Email is required' });
    }

    if (!password) {
      return res.status(401).json({ password: 'Password is required' });
    }

    const emailIsAvailable = await userService.isEmailAvailable(email);

    if (!emailIsAvailable) {
      return res.status(401).json({ email: 'This email is taken' });
    }

    const { id } = await userService.create(email, password);

    return res.json({
      token: authService.generateToken({ id }),
      refreshToken: authService.generateRefreshToken({ id }),
    });
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const loginUser = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;

    if (!email) {
      return res.status(401).json({ error: 'Email is required' });
    }

    if (!password) {
      return res.status(401).json({ error: 'Password is required' });
    }

    const emailIsAvailable = await userService.isEmailAvailable(email);

    if (emailIsAvailable) {
      return res.status(401).json({ email: 'No user found' });
    }

    const user = await userService.authenticateUser(email, password);

    if (!user) {
      return res.status(401).json({ password: 'Wrong password' });
    }

    return res.json({
      token: authService.generateToken({ id: user.id }),
      refreshToken: authService.generateRefreshToken({ id: user.id }),
    });
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const refresh = async (req: Request, res: Response) => {
  try {
    const { token } = req;
    const payload = await authService.verifyRefreshToken(token);

    if (payload && payload['id']) {
      const user = await userService.getById(payload['id']);

      if (!user) {
        return res.status(401).json({ error: 'no user found' });
      }

      return res.json({
        token: authService.generateToken({ id: user.id }),
        refreshToken: authService.generateRefreshToken({ id: user.id }),
      });
    } else {
      return res.status(401).json({ error: 'bad request' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};
