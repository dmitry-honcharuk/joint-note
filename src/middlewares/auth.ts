import { Request, Response, NextFunction } from 'express';

import authService from '../services/auth-service';
import * as userService from '../services/user-service';

import { IInstance } from '../database/models/user';

declare global {
  namespace Express {
    interface Request {
      token?: string;
      user?: IInstance;
      tokenPayload?: string | object;
    }
  }
}

export const withToken = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.header('Authorization') || '';
  const [, token] = authHeader.split(' ');

  if (!token) {
    return res.status(401).json({ error: 'no token provided' });
  }

  req.token = token;
  return next();
};

export const verifyToken = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { token } = req;

    if (!token) {
      return res.status(401).json({ error: 'no token provided' });
    }

    req.tokenPayload = await authService.verifyToken(token);
    return next();
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const getUserFromToken = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { tokenPayload } = req;

    if (tokenPayload && tokenPayload['id']) {
      const user = await userService.getById(tokenPayload['id']);

      if (!user) {
        return res.status(401).json({ error: 'no user found' });
      }

      req.user = user;
      return next();
    } else {
      return res.status(401).json({ error: 'bad request' });
    }
  } catch ({ message }) {
    return res.status(500).json({ error: message });
  }
};

export const onlyAuthenticated = () => [
  withToken,
  verifyToken,
  getUserFromToken,
];
