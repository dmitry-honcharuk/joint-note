import { Op } from 'sequelize';
import Bluebird from 'bluebird';

import { IInstance } from '../../database/models/user';
import { compare, hashString } from '../../utils/hash';
import userRepository from '../../repositories/user';
import noteRepository from '../../repositories/note';
import collaboratorRepository from '../../repositories/collaborator';

export function gatAll() {
  return userRepository.findAll();
}

export function getById(id: number) {
  return userRepository.findById(id);
}

export function getByEmail(email: string, attributes = []) {
  return userRepository.findOne({
    where: { email },
    ...(attributes.length && { attributes }),
  });
}

export async function isEmailAvailable(email: string): Bluebird<boolean> {
  const user = await getByEmail(email);

  return !(user);
}

export async function create(email: string, password: string) {
  const passwordHash = await hashString(password);

  return await userRepository.create({
    email,
    password: passwordHash,
  });
}

export async function updateById(id: number, fields: object): Promise<boolean> {
  const [affectedRows] = await userRepository.update(fields, {
    where: { id },
  });

  return !affectedRows;
}

export async function deleteById(id: number): Promise<boolean> {
  const affectedRows = await userRepository.delete({ where: { id } });

  return !affectedRows;
}

export async function authenticateUser(
  email: string,
  password: string,
): Promise<IInstance> {
  const user = await getByEmail(email);

  const isPasswordValid: boolean = await compare(password, user.password);

  if (!isPasswordValid) {
    return null;
  }

  return user;
}

export function searchByEmail(
  emailQuery: string,
  attributes: string[] = [],
  exclude: number[] = [],
  limit: number = 5,
): Bluebird<IInstance[]> {
  return userRepository.findAll({
    where: {
      email: {
        [Op.like]: `%${emailQuery}%`,
      },
      id: {
        [Op.notIn]: exclude,
      },
    },
    ...(attributes.length && { attributes }),
    limit,
  });
}

export function getSharedNotes(userId: number) {
  return noteRepository.findAll({
    include: [{
      model: noteRepository.getModel(),
      as: 'Creator',
      required: true,
      through: {
        model: collaboratorRepository.getModel(),
        attributes: [],
        where: {
          UserId: userId,
        },
      },
    }],
  });
}
