import * as jwt from 'jsonwebtoken';

import {
  TOKEN_SECRET,
  TOKEN_REFRESH_SECRET,
  TOKEN_EXPIRATION,
  REFRESH_TOKEN_EXPIRATION,
} from '../../consts/auth';

interface authOptions {
  tokenExparation?: string | number;
  refreshTokenExparation?: string | number;
}

abstract class IAuthService {
  abstract generateToken(payload: any, options: jwt.SignOptions): string;
  abstract generateRefreshToken(payload: any, options: jwt.SignOptions): string;
  abstract verifyToken(token: string);
  abstract verifyRefreshToken(token: string);
}

class AuthService extends IAuthService {
  private secret: string = TOKEN_SECRET;
  private refreshSecret: string = TOKEN_REFRESH_SECRET;
  private tokenExparation: string | number = TOKEN_EXPIRATION;
  private refreshTokenExparation: string | number = REFRESH_TOKEN_EXPIRATION;

  public generateToken(payload: any, options: jwt.SignOptions = {}): string {
    return this._generateToken(
      payload,
      this._verifyExpiration(options, this.tokenExparation),
      this.secret,
    );
  }

  public generateRefreshToken(payload: any, options: jwt.SignOptions = {}) {
    return this._generateToken(
      payload,
      this._verifyExpiration(options, this.refreshTokenExparation),
      this.refreshSecret,
    );
  }

  public async verifyToken(token: string) {
    try {
      return await this._verifyToken(token, this.secret);
    } catch ({ message }) {
      let errorMessage = message;
      if (message === 'jwt expired') {
        errorMessage = 'token expired';
      }
      throw new Error(errorMessage);
    }
  }

  public async verifyRefreshToken(token: string) {
    try {
      return await this._verifyToken(token, this.refreshSecret);
    } catch ({ message }) {
      let errorMessage = message;
      if (message === 'jwt expired') {
        errorMessage = 'refresh token expired';
      }
      throw new Error(errorMessage);
    }
  }

  private _generateToken(
    payload: any,
    options: jwt.SignOptions,
    secret: string,
  ) {
    return jwt.sign(payload, secret, options);
  }

  private _verifyExpiration(
    options: jwt.SignOptions,
    defaultExpiration: string | number = this.tokenExparation,
  ) {
    if (!options.expiresIn) {
      options.expiresIn = defaultExpiration;
    }

    return options;
  }

  private async _verifyToken(token, secret) {
    if (!token) {
      throw new Error('token was not provided');
    }

    return jwt.verify(token, secret);
  }
}

export default new AuthService();
