import userRepository from '../../repositories/user';
import collaboratorRepository from '../../repositories/collaborator';

export function createCollaborators(
  collaboratorsData: Array<{ userId: number; noteId: number }>,
) {
  return collaboratorRepository.bulkCreate(
    collaboratorsData.map(({ userId, noteId }) => ({
      NoteId: noteId,
      UserId: userId,
    })),
  );
}

export function getNoteCollaborators({
  noteId,
  attributes = [],
  includeUserInfo = false,
  userAttributes = [],
}: {
  noteId: number;
  attributes?: string[];
  includeUserInfo?: boolean;
  userAttributes?: string[];
}) {
  return collaboratorRepository.findAll({
    where: {
      NoteId: noteId,
    },
    ...(attributes.length && { attributes }),
    ...(includeUserInfo && {
      include: [
        {
          model: userRepository.getModel(),
          ...(userAttributes.length && { attributes: userAttributes }),
        },
      ],
    }),
  });
}

export function removeCollaborator(noteId, userId) {
  return collaboratorRepository.delete({
    where: {
      NoteId: noteId,
      UserId: userId,
    },
  });
}

export async function isUserCollaborator(noteId: number, userId: number) {
  const collaborators = await getNoteCollaborators({
    noteId,
    attributes: ['UserId'],
  });
  const collaboratorsIds = collaborators.map(({ UserId }) => UserId);

  return collaboratorsIds.includes(userId);
}
