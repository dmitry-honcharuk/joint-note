import noteRepository from '../../repositories/note';
import collaboratorRepository from '../../repositories/collaborator';
import userRepository from '../../repositories/user';

export function createUserNote(creatorId: number, name: string = null) {
  return noteRepository.create({
    CreatorId: creatorId,
    name,
  });
}

export function getUserNotes(
  userId: number,
  attributes: string[] = ['id', 'name', 'createdAt'],
) {
  return noteRepository.findAll({
    attributes,
    order: [['createdAt', 'DESC']],
    where: {
      CreatorId: userId,
    },
  });
}

interface INote {
  noteId: number;
  attributes?: string[];
  includeCreator?: boolean;
  creatorAttributes?: string[];
}
export function getNoteById({
  noteId,
  attributes = ['id', 'name', 'createdAt'],
  includeCreator = false,
  creatorAttributes = [],
}: INote) {
  return noteRepository.findOne({
    attributes,
    order: [['createdAt', 'DESC']],
    where: {
      id: noteId,
    },
    ...(includeCreator && {
      include: [
        {
          model: userRepository.getModel(),
          as: 'Creator',
          attributes: creatorAttributes,
        },
      ],
    }),
  });
}

export async function deleteNoteById(noteId: number): Promise<boolean> {
  const deletedRows = await noteRepository.delete({
    where: {
      id: noteId,
    },
  });
  return deletedRows > 0;
}

export async function isUserCreator(noteId: number, userId: number) {
  const note = await getNoteById({
    noteId: +noteId,
    attributes: ['CreatorId'],
  });

  return userId === note.CreatorId;
}

export function getSharedNotes(userId: number) {
  return noteRepository.findAll({
    attributes: ['id', 'name', 'createdAt'],
    include: [
      {
        model: collaboratorRepository.getModel(),
        attributes: [],
        where: {
          UserId: userId,
        },
      },
      {
        model: userRepository.getModel(),
        as: 'Creator',
        attributes: ['email'],
      },
    ],
  });
}
