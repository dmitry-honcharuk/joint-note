import * as socketIO from 'socket.io';
import { Server } from 'http';

import SocketInstance from './socket';

interface ISocketService {
  onConnect(handler: (socket: SocketInstance) => void): void;
}

class SocketService implements ISocketService {
  private io: socketIO.Server;

  constructor(server: Server) {
    this.io = socketIO(server);
  }

  public onConnect(handler: (socketInstance: SocketInstance) => void) {
    this.io.on('connection', async (socket: socketIO.Socket) => {
      const socketInstance = new SocketInstance(socket);

      socketInstance.subscribe('authenticate', data => {
        console.log(data);
      });

      handler(socketInstance);
    });
  }
}

export default SocketService;
