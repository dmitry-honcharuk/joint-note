import { Socket } from 'socket.io';

interface ISocketInstance {
  emit(event: string, data: any): void;
  subscribe(event: string, handler: (...args: any[]) => void);
  use(handler: (socket: SocketInstance) => void): void;
}

class SocketInstance implements ISocketInstance {
  private socket: Socket;
  private isAuthenticated: boolean = false;

  constructor(instance: Socket) {
    this.socket = instance;
  }

  emit(event: string, data?: any) {
    this.socket.emit(event, data);
  }

  subscribe(event: string, handler: (...args: any[]) => void) {
    this.socket.on(event, (...data) => {
      handler(...data);
    });
  }

  use(handler: (socket: SocketInstance) => void) {
    handler(this);
  }
}

export default SocketInstance;
