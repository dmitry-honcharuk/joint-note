import itemRepository from '../../repositories/item';

export function getNoteItems(
  noteId: number,
  attributes: string[] = [],
) {
  return itemRepository.findAll({
    where: {
      NoteId: noteId,
    },
    ...(attributes.length && ({ attributes })),
    order: [
      ['createdAt', 'DESC'],
    ],
  });
}

export function createItems(records: string[], noteId: number) {
  return itemRepository.bulkCreate(
    records.map(record => ({
      title: record,
      completed: false,
      NoteId: noteId,
    })),
  );
}

export function completeItem(itemId: number) {
  return itemRepository.update(
    {
      completed: true,
    },
    {
      where: {
        id: itemId,
      },
    },
  );
}

export function undoItem(itemId: number) {
  return itemRepository.update(
    {
      completed: false,
    },
    {
      where: {
        id: itemId,
      },
    },
  );
}
