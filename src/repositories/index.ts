import * as Sequelize from 'sequelize';
import * as Bluebird from 'bluebird';

class Repository<
  TModel extends Sequelize.Model<any, any>,
  TInstance,
  TAttributes
> {
  readonly model: TModel;

  constructor(model: TModel) {
    this.model = model;
  }

  getModel(): TModel {
    return this.model;
  }

  findAll(
    options?: Sequelize.FindOptions<TAttributes>,
  ): Bluebird<Array<TInstance>> {
    return this.model.findAll(options);
  }

  findOne(
    options: Sequelize.FindOptions<TAttributes>,
  ): Bluebird<TInstance | null> {
    return this.model.findOne(options);
  }

  findById(id: number): Bluebird<TInstance | null> {
    return this.model.findById(id);
  }

  create(
    values?: TAttributes,
    options?: Sequelize.CreateOptions,
  ): Bluebird<TInstance> {
    return this.model.create(values, options);
  }

  bulkCreate(
    records: TAttributes[],
    options?: Sequelize.BulkCreateOptions,
  ): Bluebird<TInstance[]> {
    return this.model.bulkCreate(records, options);
  }

  findOrCreate(options: Sequelize.FindOrInitializeOptions<TAttributes>) {
    return this.model.findOrCreate(options);
  }

  update(
    fields: TAttributes,
    updateOptions: Sequelize.UpdateOptions,
  ): Bluebird<[number, TInstance[]]> {
    return this.model.update(fields, updateOptions);
  }

  delete(options?: Sequelize.DestroyOptions): Bluebird<number> {
    return this.model.destroy(options);
  }
}

export default Repository;
