import Repository from './index';
import Model, {
  IAttributes,
  IInstance,
  IModel,
} from '../database/models/user';

export default new Repository<
  IModel,
  IInstance,
  IAttributes
>(Model);
