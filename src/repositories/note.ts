import Repository from './index';
import Model, {
  IAttributes,
  IInstance,
  IModel,
} from '../database/models/note';

export type ModelAttributes = IAttributes;
export type ModelInstance = IInstance;

export default new Repository<
  IModel,
  IInstance,
  IAttributes
>(Model);
