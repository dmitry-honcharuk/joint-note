import Repository from './index';
import Model, {
  IAttributes,
  IInstance,
  IModel,
} from '../database/models/collaborator';

export default new Repository<
  IModel,
  IInstance,
  IAttributes
>(Model);
