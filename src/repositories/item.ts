import Repository from './index';
import Model, {
  IAttributes,
  IInstance,
  IModel,
} from '../database/models/item';

export default new Repository<
  IModel,
  IInstance,
  IAttributes
>(Model);
