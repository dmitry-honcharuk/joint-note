import env from '../../config/env';

module.exports = {
  development: {
    username: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    host: env.DB_HOST,
    dialect: 'mysql',
    retry: {
      match: /ECONNREFUSED/i,
      max: 5
    },
  },
  test: {
    username: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    host: '127.0.0.1',
    dialect: 'mysql',
  },
  production: {
    username: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    host: '127.0.0.1',
    dialect: 'mysql',
  },
};
