import * as Sequelize from 'sequelize';
import sequelize, { IBasicModel } from './index';

// fields of a single database row
export interface IAttributes extends IBasicModel {
  email?: string;
  password?: string;
}

// a single database row
export type IInstance = Sequelize.Instance<IAttributes> & IAttributes;

// a table in the database
export interface IModel extends Sequelize.Model<IInstance, IAttributes> {}

const fields = {
  email: Sequelize.STRING,
  password: Sequelize.STRING,
};

const User: IModel = sequelize.define<IInstance, IAttributes>('User', fields);

export default User;
