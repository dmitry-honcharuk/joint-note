import * as Sequelize from 'sequelize';

import sequelize, { IBasicModel } from './index';
import Note from './note';

// fields of a single database row
export interface IAttributes extends IBasicModel {
  title?: string;
  completed?: boolean;
  NoteId?: number;
}

export type IInstance = Sequelize.Instance<IAttributes> & IAttributes;

// a table in the database
export interface IModel extends Sequelize.Model<IInstance, IAttributes> {}

const fields = {
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  completed: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  NoteId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'Notes',
      key: 'id',
      allowNull: false,
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
  },
};

const Item: IModel = sequelize.define<IInstance, IAttributes>('Item', fields);

Item.belongsTo(Note, { onUpdate: 'cascade', onDelete: 'cascade' });
Note.hasMany(Item, { onUpdate: 'cascade', onDelete: 'cascade' });

export default Item;
