import * as Sequelize from 'sequelize';

import sequelize, { IBasicModel } from './index';
import User from './user';

// fields of a single database row
export interface IAttributes extends IBasicModel {
  name?: string;
  CreatorId?: number;
}

export type IInstance = Sequelize.Instance<IAttributes> & IAttributes;

// a table in the database
export interface IModel extends Sequelize.Model<IInstance, IAttributes> {}

const fields = {
  name: Sequelize.STRING,
  CreatorId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'Users',
      key: 'id',
      allowNull: false,
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
  },
};

const Note: IModel = sequelize.define<IInstance, IAttributes>('Note', fields);

User.hasMany(Note, {
  as: 'Creator',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});

Note.belongsTo(User, { as: 'Creator' });

export default Note;
