import * as Sequelize from 'sequelize';

import sequelize, { IBasicModel } from './index';

import Note from './note';
import User from './user';

// fields of a single database row
export interface IAttributes extends IBasicModel {
  NoteId?: number;
  UserId?: number;
}

export type IInstance = Sequelize.Instance<IAttributes> & IAttributes;

// a table in the database
export interface IModel extends Sequelize.Model<IInstance, IAttributes> {}

const fields = {
  NoteId: Sequelize.INTEGER,
  UserId: Sequelize.INTEGER,
};

const Collaborator: IModel = sequelize.define<IInstance, IAttributes>(
  'Collaborator',
  fields,
);

Note.hasMany(Collaborator, { onUpdate: 'cascade', onDelete: 'cascade' });
Collaborator.belongsTo(User, { onUpdate: 'cascade', onDelete: 'cascade' });

export default Collaborator;
