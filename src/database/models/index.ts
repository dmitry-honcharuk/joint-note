import * as Sequelize from 'sequelize';
import env from '../../config/env';

export interface IBasicModel {
  id?: number;
  createdAt?: string;
  updatedAt?: string;
}

const sequelize = new Sequelize(env.DB_NAME, env.DB_USER, env.DB_PASSWORD, {
  host: env.DB_HOST,
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connected');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

export default sequelize;
