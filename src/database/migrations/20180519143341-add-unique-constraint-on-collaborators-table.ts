import { QueryInterface } from 'sequelize';

export default {
  up: async (queryInterface: QueryInterface, Sequelize) => {
    await queryInterface.addConstraint('Collaborators', ['NoteId', 'UserId'], {
      type: 'unique',
      name: 'collaborators_unique_key',
    });
  },
  down: async (queryInterface: QueryInterface) => {
  },
};
