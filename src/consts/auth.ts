import env from '../config/env';

export const TOKEN_SECRET = env.TOKEN_SECRET;
export const TOKEN_REFRESH_SECRET = env.TOKEN_REFRESH_SECRET;
export const TOKEN_EXPIRATION = env.TOKEN_EXPIRATION;
export const REFRESH_TOKEN_EXPIRATION = env.REFRESH_TOKEN_EXPIRATION;
