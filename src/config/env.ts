import { config } from 'dotenv';

config();

// @ts-ignore: Cannot find namespace 'process'
export default process.env;
