import * as express from 'express';
import * as path from 'path';
import { Server } from 'http';
import * as morgan from 'morgan';
import * as helmet from 'helmet';

import env from './config/env';
import { initSocketServer } from './socket-server';
import apiRoutes from './routes/api';

const PORT = env.SERVER_PORT || 3000;
const app = express();
const server = new Server(app);

initSocketServer(server);

// Config
app.use(helmet());
app.use(morgan('dev'));
app.use(express.json());

app.use('/public', express.static(__dirname + '/../public'));
app.use(express.static(__dirname + '/../dist'));

// Routes
app.use('/api', apiRoutes);

app.get('*', (req, res) => {
  return res.sendFile(path.resolve(__dirname, '../index.html'));
});

// Server start
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
