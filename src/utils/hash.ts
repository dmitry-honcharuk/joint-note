import * as bcrypt from 'bcryptjs';

export const hashString = (
  string: string,
  saltRounds: number = 10,
): Promise<string> => {
  return bcrypt.hash(string, saltRounds);
};

export const compare = (
  string: string,
  hashedString: string,
): Promise<boolean> => {
  return bcrypt.compare(string, hashedString);
};
