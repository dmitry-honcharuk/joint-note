FROM node:carbon

# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
COPY package.json /tmp/app/package.json
RUN cd /tmp/app && npm install
RUN mkdir -p /var/www/joint-note && ln -s /tmp/app/node_modules /var/www/joint-note/node_modules

WORKDIR /var/www/joint-note

COPY ./ ./

CMD ["/bin/bash"]
