export const CREATE_NOTE_FORM_NAME = 'CREATE_NOTE';

export const TABS = {
  PERSONAL: 'personal',
  SHARED: 'shared',
};
