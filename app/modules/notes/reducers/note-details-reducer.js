import { COMPLETE_ITEM, ITEM_ADDED, UNDO_ITEM } from '@modules/items/action-names';
import { GET_COLLABORATORS, GET_NOTE, REMOVE_COLLABORATOR } from '../action-names';

import { getNoteDisplayName } from '@services/notes-service';

const initialState = {
  note: {
    id: 0,
    creatorId: 0,
    name: '',
    creator: {},
  },
  collaborators: [],
  items: [],
};

const getCompletedItem = item => ({
  ...item,
  completed: true,
});

const getInCompletedItem = item => ({
  ...item,
  completed: false,
});

export default function notesReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_NOTE.SUCCESS:
      return {
        ...state,
        note: {
          ...payload.note,
          name: getNoteDisplayName(payload.note),
          creatorId: payload.note.CreatorId,
          creator: payload.note.Creator,
        },
        collaborators: payload.collaborators,
        items: payload.items,
      };
    case COMPLETE_ITEM:
      return {
        ...state,
        items: state.items.map(
          item => (item.id === payload.id ? getCompletedItem(item) : item),
        ),
      };
    case UNDO_ITEM:
      return {
        ...state,
        items: state.items.map(
          item => (item.id === payload.id ? getInCompletedItem(item) : item),
        ),
      };
    case GET_COLLABORATORS.SUCCESS:
      return {
        ...state,
        collaborators: payload.collaborators,
      };
    case REMOVE_COLLABORATOR.SUCCESS:
      return {
        ...state,
        collaborators: state.collaborators.filter(
          c => c.UserId !== payload.userId,
        ),
      };
    case ITEM_ADDED:
      return {
        ...state,
        items: [payload.item, ...state.items],
      };
    default:
      return state;
  }
}
