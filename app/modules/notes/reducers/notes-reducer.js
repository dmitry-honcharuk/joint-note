import { CREATE_NOTE, GET_NOTES, REMOVE_NOTE_FROM_LIST } from '../action-names';

import { getNoteDisplayName } from '@services/notes-service';

const initialState = [];

const processNote = note => ({
  id: note.id,
  name: getNoteDisplayName(note),
  createdAt: note.createdAt,
});

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_NOTES.SUCCESS:
      return payload.notes.map(processNote);

    case CREATE_NOTE.SUCCESS:
      return [processNote(payload.note), ...state];

    case REMOVE_NOTE_FROM_LIST:
      return state.filter(({ id }) => id !== payload.noteId);

    default:
      return state;
  }
}
