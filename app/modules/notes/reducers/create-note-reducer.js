import { CREATE_NOTE_DIALOG } from '../action-names';

const initialState = {
  isDialogOpen: false,
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case CREATE_NOTE_DIALOG.OPEN:
      return {
        ...state,
        isDialogOpen: true,
      };
    case CREATE_NOTE_DIALOG.CLOSE:
      return {
        ...state,
        isDialogOpen: false,
      };
    default:
      return state;
  }
}
