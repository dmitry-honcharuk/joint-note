import { GET_SHARED_NOTES, STOP_COLLABORATION } from '@modules/notes/action-names';
import { getNoteDisplayName } from '@services/notes-service';

const initialState = [];

const processNote = note => ({
  id: note.id,
  name: getNoteDisplayName(note),
  createdAt: note.createdAt,
  creator: note.Creator,
});

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_SHARED_NOTES.SUCCESS:
      return payload.notes.map(processNote);

    case STOP_COLLABORATION.SUCCESS:
      return state.filter(({ id }) => id !== payload.noteId);

    default:
      return state;
  }
}
