import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { compose, withStateHandlers } from 'recompose';

import { Layout } from '@modules/common';
import PersonalNotesList from '../components/personal-notes-list';
import SharedNotesList from '../components/shared-notes-list';
import ActionsPanel from '../components/actions-panel';
import CreateNoteDialog from '../components/create-note-dialog';

import { TABS } from '../consts';

const styles = theme => ({
  actionsPanel: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  },
});

const Dashboard = ({
  classes,
  tabs,
  currentTab,
  onTabChange,
}) => (
  <Layout>
    <div className={classes.actionsPanel}>
      <ActionsPanel
        tabs={tabs}
        currentTab={currentTab}
        onTabChange={onTabChange}
      />
    </div>
    {currentTab === TABS.PERSONAL && <PersonalNotesList />}
    {currentTab === TABS.SHARED && <SharedNotesList />}
    <CreateNoteDialog />
  </Layout>
);

export default compose(
  withStateHandlers(
    {
      tabs: [
        TABS.PERSONAL,
        TABS.SHARED,
      ],
      currentTab: TABS.PERSONAL,
    },
    {
      onTabChange: (state) => (newTab) => ({
        ...state,
        currentTab: newTab,
      }),
    },
  ),
  withStyles(styles),
)(Dashboard);
