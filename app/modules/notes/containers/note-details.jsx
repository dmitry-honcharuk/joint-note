import React, { Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import { Layout } from '@modules/common';
import { getNote } from '@modules/notes/actions';

import NoteDetails from '../components/note-details';

const NoteDetailsRoute = () => (
  <Layout>
    <NoteDetails />
  </Layout>
);

const mapDispatchToProps = { getNote };

export default compose(
  withRouter,
  connect(null, mapDispatchToProps),
  lifecycle({
    componentDidMount() {
      const {
        params: { noteId },
      } = this.props.match;

      this.props.getNote(noteId);
    },
  }),
)(NoteDetailsRoute);
