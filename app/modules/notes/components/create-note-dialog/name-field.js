import React from 'react';
import TextField from '@material-ui/core/TextField';

export default ({
  input,
  label,
  meta: { touched, error },
}) => (
  <TextField
    autoFocus
    margin="dense"
    fullWidth
    error={!!(touched && error)}
    helperText={touched && error && <span>{error}</span>}
    {...input}
  />
);
