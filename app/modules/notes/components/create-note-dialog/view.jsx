import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import NameField from './name-field';

const propTypes = {
  isDialogOpen: PropTypes.bool,
  closeDialog: PropTypes.func.isRequired,
  createNote: PropTypes.func.isRequired,
};
const defaultProps = {
  isDialogOpen: false,
};

const CreateNoteDialog = ({ isDialogOpen, closeDialog, createNote, handleSubmit }) => ((
  <Dialog
    open={isDialogOpen}
    onClose={closeDialog}
    aria-labelledby="create-note-dialog-title"
  >
    <DialogTitle id="create-note-dialog-title">New Note</DialogTitle>
    <form
      onSubmit={() => null}
      onKeyPress={e => e.key === 'Enter' && e.preventDefault()}>
      <DialogContent>
        <div>
          <Typography variant="subheading">
            Please enter name for your new note.
          </Typography>
          <Typography variant="caption">
            Leave empty to set current date and time as a name.
          </Typography>
        </div>
        <Field
          name="name"
          component={NameField}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={closeDialog}
          color="primary"
          type="reset"
        >
          Cancel
        </Button>
        <Button
          onClick={handleSubmit(createNote)}
          color="primary"

          type="submit"
        >
          Create
        </Button>
      </DialogActions>
    </form>
  </Dialog>
));

CreateNoteDialog.propTypes = propTypes;
CreateNoteDialog.defaultProps = defaultProps;

export default CreateNoteDialog;
