import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { compose } from 'recompose';

import CreateNoteDialog from './view';

import { closeCreateNoteDialog, createNote } from '../../actions';
import { CREATE_NOTE_FORM_NAME } from '../../consts';

const mapStateToProps = ({ createNoteReducer: { isDialogOpen } }) => ({
  isDialogOpen,
  initialValues: {
    name: '',
  },
});

const mapDispatchToProps = {
  createNote,
  closeDialog: closeCreateNoteDialog,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: CREATE_NOTE_FORM_NAME,
  }),
)(CreateNoteDialog);
