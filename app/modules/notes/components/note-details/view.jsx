import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import { ItemsList } from '../items-list';
import { AddItemForm } from '@modules/items';

// @TODO Collaborators

import s from './styles';

const propTypes = {
  name: PropTypes.string,
  items: PropTypes.array,
  creator: PropTypes.shape({
    email: PropTypes.string.isRequired,
  }),
};

const defaultProps = {
  name: '',
  items: [],
  creator: {
    email: '',
  },
};

const NoteDetails = ({
  note: {
    name,
    creator: { email: creatorEmail },
  },
  removeNote,
  items,
  classes,
}) => (
  <div className={classes.container}>
    <div className={classes.header}>
      <div className={classes.info}>
        <h1 className={classes.title}>
          <div>{name}</div>
          <div className={classes.creator}>{creatorEmail}</div>
        </h1>
      </div>
      <div className={classes.actions}>
        <Button
          color="secondary"
          size="small"
          className={classes.action}
          onClick={removeNote}
        >
          Remove note
        </Button>
      </div>
    </div>
    {/*<SearchUsersForm />*/}
    {/*<CollaboratorPretendersList />*/}
    {/*<NoteCollaborators />*/}
    <div className={classes.contentWrapper}>
      <div className={classes.content}>
        <AddItemForm />
        <Paper
          className={cn(classes.itemsPaper, {
            [classes.empty]: !items.length,
          })}
          elevation={1}
        >
          {items.length ? (
            <ItemsList items={items} />
          ) : (
            <Typography align={'center'} variant={'subheading'}>
              No items in this note yet
            </Typography>
          )}
        </Paper>
      </div>
    </div>
  </div>
);

NoteDetails.propTypes = propTypes;
NoteDetails.defaultProps = defaultProps;

export default withStyles(s)(NoteDetails);
