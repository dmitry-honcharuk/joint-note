export default theme => ({
  container: {
    marginTop: theme.spacing.unit * 2,
  },
  header: {
    display: 'flex',
    alignItems: 'flex-start',
  },
  info: {
    flexGrow: 5,
  },
  actions: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  action: {
    '&:not(:first-of-type)': {
      marginLeft: theme.spacing.unit,
    },
  },
  title: {
    ...theme.typography.display2,
    marginTop: 0,
    marginBottom: 0,
  },
  creator: {
    ...theme.typography.caption,
  },
  itemList: {
    listStyle: 'none',
    paddingLeft: 0,
  },
  contentWrapper: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  content: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 'auto',
      minWidth: 500,
    },
  },
  itemsPaper: {
    marginTop: theme.spacing.unit * 2,
  },
  empty: {
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
});
