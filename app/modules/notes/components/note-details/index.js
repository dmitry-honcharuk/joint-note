import { connect } from 'react-redux';

import NoteDetailView from './view';

import { completeItem, undoItem } from '@modules/items/actions';

import { deleteNote } from '../../actions';

const mapStateToProps = ({ noteReducer: { note, collaborators, items } }) => ({
  note,
  collaborators,
  items,
});

const mapDispatchToProps = {
  completeItem,
  undoItem,
  removeNote: deleteNote,
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetailView);
