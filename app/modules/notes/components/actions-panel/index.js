import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import ActionsPanel from './view';

import { openCreateNoteDialog } from '../../actions';

const styles = theme => ({
  panel: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tabButton: {
    '&:not(:last-of-type)': {
      marginRight: theme.spacing.unit,
    },
  },
});

export default compose(
  withStyles(styles),
  connect(null, {
    openCreateNoteDialog,
  }),
)(ActionsPanel);
