import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Button from '@material-ui/core/Button';
import { openCreateNoteDialog } from "@modules/notes/actions";

const propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.string),
  currentTab: PropTypes.string,
  onTabChange: PropTypes.func.isRequired,
};
const defaultProps = {
  tabs: [],
  currentTab: '',
};

const ActionsPanel = ({ tabs, currentTab, onTabChange, openCreateNoteDialog, classes }) => (
  <div className={classes.panel}>
    <div>
      {tabs.map(tab => (
        <Button
          className={classes.tabButton}
          variant={currentTab === tab ? 'outlined' : 'text'}
          color="primary"
          key={tab}
          onClick={() => onTabChange(tab)}
        >{tab}</Button>
      ))}
    </div>
    <Button
      color="secondary"
      onClick={openCreateNoteDialog}
    >create</Button>
  </div>
);

ActionsPanel.propTypes = propTypes;
ActionsPanel.defaultProps = defaultProps;

export default ActionsPanel
