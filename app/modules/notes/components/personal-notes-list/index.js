import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import DeleteIcon from '@material-ui/icons/Delete';

import NotesList from '../notes-list';
import { deleteNote, getNotes } from '../../actions';

import { DELETE_NOTE, NOTE_DATE_FORMAT } from '@consts/notes';

const mapStateToProps = ({ notesReducer }) => ({
  notes: notesReducer.map(note => ({
    ...note,
    createdAt: moment(note.createdAt).format(NOTE_DATE_FORMAT),
  })),
  cardDropdownItems: [
    {
      id: DELETE_NOTE,
      label: (
        <DeleteIcon />
      ),
    },
  ],
});

const mapDispatchToProps = {
  getNotes,
  removeNote: deleteNote,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  lifecycle({
    componentDidMount() {
      this.props.getNotes();
    },
  }),
)(NotesList);
