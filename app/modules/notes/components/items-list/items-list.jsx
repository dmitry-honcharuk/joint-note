import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

import { changeItemState } from '@modules/items/actions';

const propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      completed: PropTypes.bool.isRequired,
    }),
  ),
  setItemState: PropTypes.func.isRequired,
};
const defaultProps = {
  items: [],
};

const ItemsList = ({ items, setItemState }) => (
  <List
    dense={true}
    disablePadding={true}
  >
    {items.map(item => (
      <ListItem
        key={item.id}
        onClick={() => setItemState(item.id, !item.completed)}
        button
        dense
      >
        <Checkbox
          checked={item.completed}
          tabIndex={-1}
          disableRipple
        />
        <ListItemText primary={item.title} />
      </ListItem>
    ))}
  </List>
);

ItemsList.propTypes = propTypes;
ItemsList.defaultProps = defaultProps;

export default connect(null, {
  setItemState: changeItemState,
})(ItemsList);
