import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Typography from '@material-ui/core/Typography';

import { DropdownMenu } from '@modules/common';

const propTypes = {
  name: PropTypes.string,
  id: PropTypes.number,
  createdAt: PropTypes.string,
  noteCardDropdownPick: PropTypes.func.isRequired,
  dropdownItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]),
    }),
  ),
  createdBy: PropTypes.string,
};
const defaultProps = {
  name: null,
  id: null,
  createdAt: null,
  createdBy: null,
};

const NoteCard = ({
  name,
  id: noteId,
  createdAt,
  dropdownItems,
  noteCardDropdownPick,
  createdBy,
  classes,
}) => (
  <Card className={classes.card}>
    <CardHeader
      action={
        <DropdownMenu
          id={`note-${noteId}-dropdown`}
          menuItems={dropdownItems}
          onItemPick={actionId => noteCardDropdownPick(actionId, noteId)}
          trigger={(
            <IconButton>
              <MoreVertIcon />
            </IconButton>
          )}
        />
      }
      title={(
        <Link
          className={classes.title}
          to={`/notes/${noteId}`}
        >{name}</Link>
      )}
      {...createdBy && { subheader: createdBy }}
    />
    <CardContent>
      <Typography
        className={classes.created}
        color="textSecondary"
      >
        {createdAt}
      </Typography>
    </CardContent>
  </Card>
);

NoteCard.propTypes = propTypes;
NoteCard.defaultProps = defaultProps;

export default NoteCard;
