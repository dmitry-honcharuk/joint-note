import React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import View from './view';

import s from './styles';
import { noteCardDropdownPick } from '../../actions';

export default compose(
  withStyles(s),
  connect(null, {
    noteCardDropdownPick,
  }),
)(View);
