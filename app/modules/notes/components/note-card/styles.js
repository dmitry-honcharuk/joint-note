export default theme => ({
  card: {
    minWidth: 300,
  },
  title: {
    ...theme.typography.title,
    textDecoration: 'none',
  },
  created: {
    ...theme.typography.caption,
    textAlign: 'right',
  },
});
