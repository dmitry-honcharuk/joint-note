import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

import { NoteCard } from '@modules/notes'

const propTypes = {
  notes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      creator: PropTypes.shape({
        email: PropTypes.string.isRequired,
      }),
    }),
  ),
  cardDropdownItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]),
    }),
  ).isRequired,
};
const defaultProps = {
  notes: [],
};

const SharedNotesList = ({ notes, cardDropdownItems, classes }) => (
  <Grid container spacing={24}>
    {notes.map(({ id, name, creator, createdAt }) => (
      <Grid item xs key={id}>
        <NoteCard
          name={name}
          id={id}
          createdAt={createdAt}
          dropdownItems={cardDropdownItems}
          createdBy={creator && creator.email}
        />
      </Grid>
    ))}
  </Grid>
);

SharedNotesList.propTypes = propTypes;
SharedNotesList.defaultProps = defaultProps;

export default SharedNotesList;
