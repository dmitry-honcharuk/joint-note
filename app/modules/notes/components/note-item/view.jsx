import React from 'react';
import PropTypes from 'prop-types';

const NoteDetails = ({ title, completed, onChange }) => (
  <label>
    <input type="checkbox" checked={completed} onChange={onChange} />
    <span>{title}</span>
  </label>
);

NoteDetails.propTypes = {
  title: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default NoteDetails;
