import { connect } from 'react-redux';

import NoteItemView from './view';

import { completeItem, undoItem } from '@modules/items/actions';

const mapDispatchToProps = (dispatch, { id }) => ({
  onChange: ({ target }) => {
    if (!target.checked) {
      dispatch(undoItem(id));
    } else {
      dispatch(completeItem(id));
    }
  },
});

export default connect(null, mapDispatchToProps)(NoteItemView);
