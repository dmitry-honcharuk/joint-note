import React from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import moment from 'moment';
import DeleteIcon from '@material-ui/icons/Delete';

import NotesList from '../notes-list';
import { getSharedNotes } from '../../actions';

import { DELETE_SHARED_NOTE, NOTE_DATE_FORMAT } from '@consts/notes';

const mapStateToProps = ({ sharedNotesReducer }) => ({
  notes: sharedNotesReducer.map(note => ({
    ...note,
    createdAt: moment(note.createdAt).format(NOTE_DATE_FORMAT),
  })),
  cardDropdownItems: [
    {
      id: DELETE_SHARED_NOTE,
      label: (
        <DeleteIcon />
      ),
    },
  ],
});

const mapDispatchToProps = {
  getSharedNotes,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  lifecycle({
    componentDidMount() {
      this.props.getSharedNotes();
    },
  }),
)(NotesList);
