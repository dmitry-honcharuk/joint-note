import { push } from 'react-router-redux';
import { getFormValues, reset } from 'redux-form';

import * as notesService from '@services/notes-service';
import { makeUserNotAllowed } from '@modules/authentication/actions';
import {
  ADD_TO_COLLABORATORS,
  CREATE_NOTE,
  CREATE_NOTE_DIALOG,
  GET_COLLABORATORS,
  GET_NOTE,
  GET_NOTES,
  GET_SHARED_NOTES,
  REMOVE_COLLABORATOR,
  REMOVE_NOTE_FROM_LIST,
  STOP_COLLABORATION,
} from './action-names';

import { CREATE_NOTE_FORM_NAME } from './consts';

import { DELETE_NOTE, DELETE_SHARED_NOTE } from '@consts/notes';

export const createNote = () => async (dispatch, getState) => {
  const {
    name,
  } = getFormValues(CREATE_NOTE_FORM_NAME)(getState());

  dispatch({ type: CREATE_NOTE.REQUEST });

  try {
    const note = await notesService.createNote(name);

    dispatch({
      type: CREATE_NOTE_DIALOG.CLOSE,
    });
    dispatch(reset(CREATE_NOTE_FORM_NAME));
    dispatch({
      type: CREATE_NOTE.SUCCESS,
      payload: {
        note,
      },
    });
  } catch ({ message }) {
    dispatch({
      type: CREATE_NOTE.FAIL,
      payload: {
        error: message,
      },
    });
  }
};

export const deleteNote = () => async (dispatch, getState) => {
  try {
    const {
      noteReducer: {
        note: { id: noteId },
      },
    } = getState();

    await notesService.deleteNoteById(noteId);
    dispatch(push('/'));
  } catch ({ message }) {
    dispatch(makeUserNotAllowed());
  }
};

export const getNotes = () => async dispatch => {
  dispatch({ type: GET_NOTES.REQUEST });

  try {
    const notes = await notesService.getPersonalNotes();

    dispatch({
      type: GET_NOTES.SUCCESS,
      payload: {
        notes,
      },
    });
  } catch ({ message }) {
    dispatch({
      type: GET_NOTES.FAIL,
      payload: {
        error: message,
      },
    });
  }
};

export const getSharedNotes = () => async dispatch => {
  dispatch({ type: GET_SHARED_NOTES.REQUEST });

  try {
    const notes = await notesService.getSharedNotes();

    dispatch({
      type: GET_SHARED_NOTES.SUCCESS,
      payload: {
        notes,
      },
    });
  } catch ({ message }) {
    dispatch({
      type: GET_SHARED_NOTES.FAIL,
      payload: {
        error: message,
      },
    });
  }
};

export const getNote = id => async dispatch => {
  dispatch({ type: GET_NOTE.REQUEST });

  try {
    const { note, collaborators, items } = await notesService.getNoteById(id);

    dispatch({
      type: GET_NOTE.SUCCESS,
      payload: {
        note,
        collaborators,
        items,
      },
    });
  } catch ({ message }) {
    dispatch(makeUserNotAllowed());
  }
};

export const addCollaborators = () => async (dispatch, getState) => {
  const {
    collaboratorPretenders,
    noteReducer: { note: { id: noteId } },
  } = getState();

  await notesService.addCollaborators(
    noteId,
    collaboratorPretenders.map(({ id }) => id),
  );

  dispatch({
    type: ADD_TO_COLLABORATORS.SUCCESS,
  });

  dispatch(getCollaborators());
};

export const getCollaborators = () => async (dispatch, getState) => {
  const {
    noteReducer: { note: { id: noteId } },
  } = getState();

  const collaborators = await notesService.getCollaborators(noteId);

  dispatch({
    type: GET_COLLABORATORS.SUCCESS,
    payload: {
      collaborators,
    },
  });
};

export const removeCollaborator = userId => async (dispatch, getState) => {
  const { noteReducer: { note: { id: noteId } } } = getState();

  await notesService.removeCollaborator(noteId, userId);

  dispatch({
    type: REMOVE_COLLABORATOR.SUCCESS,
    payload: {
      userId: userId,
    },
  });
};

export const noteCardDropdownPick = (actionId, noteId) => async (dispatch, getState) => {
  switch (actionId) {
    case DELETE_NOTE:
      try {
        await notesService.deleteNoteById(noteId);
        return dispatch({
          type: REMOVE_NOTE_FROM_LIST,
          payload: {
            noteId,
          },
        });
      } catch ({ message }) {
        return dispatch(makeUserNotAllowed());
      }
    case DELETE_SHARED_NOTE:
      try {
        await notesService.stopCollaborating(noteId);
        return dispatch({
          type: STOP_COLLABORATION.SUCCESS,
          payload: {
            noteId,
          },
        });
      } catch ({ message }) {
        return dispatch(makeUserNotAllowed());
      }
  }
};

export const openCreateNoteDialog = () => ({
  type: CREATE_NOTE_DIALOG.OPEN,
});

export const closeCreateNoteDialog = () => dispatch => {

  dispatch({
    type: CREATE_NOTE_DIALOG.CLOSE,
  });
  dispatch(reset(CREATE_NOTE_FORM_NAME));
};
