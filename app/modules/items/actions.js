import { getFormValues, change } from 'redux-form';

import * as itemsService from '../../services/items-service';
import { COMPLETE_ITEM, ITEM_ADDED, UNDO_ITEM } from './action-names';

import { NEW_ITEM_FORM_NAME } from './consts';

export const completeItem = itemId => async dispatch => {
  await itemsService.completeItem(itemId);
  dispatch({
    type: COMPLETE_ITEM,
    payload: {
      id: itemId,
    },
  });
};

export const undoItem = itemId => async dispatch => {
  await itemsService.undoItem(itemId);
  dispatch({
    type: UNDO_ITEM,
    payload: {
      id: itemId,
    },
  });
};

export const changeItemState = (itemId, complete) => async dispatch => {
  if (complete) {
    dispatch(completeItem(itemId));
  } else {
    dispatch(undoItem(itemId));
  }
};

export const addItem = () => async (dispatch, getState) => {
  const currentState = getState();

  const {
    noteReducer: {
      note: { id: noteId },
    },
  } = currentState;
  const {
    title,
  } = getFormValues(NEW_ITEM_FORM_NAME)(currentState);

  if (title && title.length) {
    dispatch(change(NEW_ITEM_FORM_NAME, 'title', ''));
    const [item] = await itemsService.addItems(noteId, [title]);

    dispatch({
      type: ITEM_ADDED,
      payload: {
        item,
      },
    });
  }
};
