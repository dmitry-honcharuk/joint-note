export default ({ spacing }) => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  input: {
    flexGrow: 1,
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginLeft: spacing.unit * 2,
  },
});
