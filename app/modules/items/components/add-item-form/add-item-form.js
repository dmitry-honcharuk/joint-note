import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { reduxForm } from 'redux-form';
import Button from '@material-ui/core/Button';
// import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import { NEW_ITEM_FORM_NAME as FORM_NAME } from '../../consts';
import { addItem } from '../../actions';

import s from './styles';

import { TextField } from '@modules/common';

const propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  addItem: PropTypes.func.isRequired,
};

const AddItemForm = ({ addItem, classes, handleSubmit }) => (
  <form
    onSubmit={handleSubmit(addItem)}
    autoComplete="off"
    className={classes.container}
  >
    <div className={classes.input}>
      <TextField
        name="title"
        label="Title"
        withErrorSpace={false}
      />
    </div>
    <div
      className={classes.button}
    >
      <Button
        variant="text"
        color="primary"
        type="submit"
        size={'small'}
      >
        Add item
      </Button>
    </div>
  </form>
);

AddItemForm.propTypes = propTypes;

const mapStateToProps = () => ({
  initialValues: {
    title: '',
  },
});
export default compose(
  withStyles(s),
  connect(
    mapStateToProps,
    {
      addItem,
    },
  ),
  reduxForm({
    form: FORM_NAME,
  }),
)(AddItemForm);
