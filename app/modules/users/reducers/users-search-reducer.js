import { LOGOUT } from '@modules/authentication/action-names';
import { ADD_TO_COLLABORATORS } from "@modules/notes/action-names";
import { SEARCH_FIELD_CHANGE, SEARCH_FIELD_CLEAR, SEARCH_USERS, } from '../action-names';

const initialState = {
  users: [],
  fetching: false,
  currentQuery: '',
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_USERS.REQUEST: {
      return {
        ...state,
        fetching: true,
      };
    }
    case SEARCH_USERS.SUCCESS:
      return {
        ...state,
        users: payload.users,
        fetching: false,
      };
    case SEARCH_USERS.FAIL:
      return {
        ...state,
        fetching: false,
      };
    case SEARCH_FIELD_CHANGE:
      return {
        ...state,
        currentQuery: payload.currentQuery,
      };
    case SEARCH_FIELD_CLEAR:
      return {
        ...state,
        users: [],
        fetching: false,
      };
    case LOGOUT:
      return {
        ...state,
        users: [],
        currentQuery: '',
        fetching: false,
      };
    case ADD_TO_COLLABORATORS.SUCCESS:
      return {
        ...state,
        users: [],
        currentQuery: '',
        fetching: false,
      };
    default:
      return state;
  }
}
