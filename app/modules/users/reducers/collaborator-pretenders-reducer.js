import { ADD_TO_COLLABORATORS } from "@modules/notes/action-names";
import { ADD_TO_COLLABORATORS_LIST, REMOVE_FROM_COLLABORATORS_LIST } from '../action-names';

const initialState = [];

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case ADD_TO_COLLABORATORS_LIST: {
      return [
        ...state,
        payload.user
      ];
    }
    case REMOVE_FROM_COLLABORATORS_LIST: {
      return state.filter(user => user.id !== payload.userId);
    }
    case ADD_TO_COLLABORATORS.SUCCESS:
      return [];
    default:
      return state;
  }
}
