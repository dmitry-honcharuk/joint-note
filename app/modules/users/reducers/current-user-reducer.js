import { LOGOUT } from '@modules/authentication/action-names';
import { GET_CURRENT_USER } from '../action-names';

const initialState = {
  id: null,
  email: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_CURRENT_USER.SUCCESS: {
      return {
        ...state,
        ...payload.user,
      };
    }
    case LOGOUT :
      return {
        ...initialState,
      };
    default:
      return state;
  }
}
