import { connect } from 'react-redux';

import { removeCollaborator } from '@modules/notes/actions';

import NoteCollaborators from './view';

const mapStateToProps = ({ noteReducer: { collaborators } }) => ({
  collaborators: collaborators.map(collaborator => ({
    id: collaborator.UserId,
    email: collaborator.User.email,
  })),
});
const mapDispatchToProps = dispatch => ({
  removeCollaborator: (id) => {
    dispatch(removeCollaborator(id));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NoteCollaborators);
