import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  collaborators: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      email: PropTypes.string.isRequired,
    }),
  ),
};

const defaultProps = {
  collaborators: [],
};

const NoteCollaborators = ({ collaborators, removeCollaborator }) => (
  <ul>
    {collaborators.map(collaborator => (
      <li key={collaborator.id}>
        <span>{collaborator.email}</span>
        <button onClick={() => removeCollaborator(collaborator.id)}>Remove</button>
      </li>
    ))}
  </ul>
);

NoteCollaborators.propTypes = propTypes;
NoteCollaborators.defaultProps = defaultProps;

export default NoteCollaborators;
