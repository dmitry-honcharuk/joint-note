import React, { Fragment } from 'react';

const CreateNote = ({ onChange, users, fetching, currentQuery, onAddUser }) => {
  return (
    <Fragment>
      <input
        type="text"
        onChange={onChange}
        value={currentQuery}
      />
      <ul>
        {users.map((user) => (
          <li key={user.id}>
            <span>{user.email}</span>
            <button onClick={() => onAddUser(user)}>+</button>
          </li>
        ))}
      </ul>
    </Fragment>
  );
};

export default CreateNote;
