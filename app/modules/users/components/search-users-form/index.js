import { compose } from 'redux';
import { connect } from 'react-redux';

import SearchUsersFormView from './view';

import { addToCollaboratorsList, searchFieldChange } from '@modules/users/actions';

const mapStateToProps = ({ usersSearchReducer }) => ({
  users: usersSearchReducer.users,
  fetching: usersSearchReducer.fetching,
  currentQuery: usersSearchReducer.currentQuery,
});

const mapDispatchToProps = dispatch => ({
  onChange: ({ target }) => {
    dispatch(searchFieldChange(target.value));
  },
  onAddUser: (user) => {
    dispatch(addToCollaboratorsList(user));
  },
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchUsersFormView,
);
