import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      email: PropTypes.string.isRequired,
    }),
  ),
};

const defaultProps = {
  users: [],
};

const CollaboratorsPretendersList = ({
  users,
  onRemove,
  addCollaborators,
}) => (
  <Fragment>
    <ul>
      {users.map(user => (
        <li key={user.id}>
          <span>{user.email}</span>
          <button onClick={() => onRemove(user.id)}>-</button>
        </li>
      ))}
    </ul>
    {users.length !== 0 && <button onClick={addCollaborators}>Add Collaborators</button>}
  </Fragment>
);

CollaboratorsPretendersList.propTypes = propTypes;
CollaboratorsPretendersList.defaultProps = defaultProps;

export default CollaboratorsPretendersList;
