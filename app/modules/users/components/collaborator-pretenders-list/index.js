import { connect } from 'react-redux';

import { removeFromCollaboratorsList } from '../../actions';
import { addCollaborators } from '@modules/notes/actions';

import View from './view';

const mapStateToProps = ({ collaboratorPretenders }) => ({ users: collaboratorPretenders });

const mapDispatchToProps = dispatch => ({
  onRemove: (userId) => {
    dispatch(removeFromCollaboratorsList(userId));
  },
  addCollaborators: () => {
    dispatch(addCollaborators());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(View);
