import * as usersService from '@services/users-service';

import {
  ADD_TO_COLLABORATORS_LIST,
  GET_CURRENT_USER,
  REMOVE_FROM_COLLABORATORS_LIST,
  SEARCH_FIELD_CHANGE,
  SEARCH_FIELD_CLEAR,
  SEARCH_USERS,
} from './action-names';

export const MIN_SEARCH_USERS_EMAIL_LENGTH = 3;

export const searchFieldChange = newQuery => (dispatch, getState) => {
  const {
    usersSearchReducer: { currentQuery, users },
  } = getState();

  dispatch({
    type: SEARCH_FIELD_CHANGE,
    payload: { currentQuery: newQuery },
  });

  if (newQuery.length >= MIN_SEARCH_USERS_EMAIL_LENGTH) {
    dispatch(searchUsersByEmail(newQuery));
  } else if (
    currentQuery.length > newQuery.length &&
    newQuery.length < MIN_SEARCH_USERS_EMAIL_LENGTH &&
    users.length !== 0
  ) {
    dispatch(clearUsersSearch());
  }
};

export const searchUsersByEmail = email => async (dispatch, getState) => {
  dispatch({ type: SEARCH_USERS.REQUEST });

  try {
    const {
      noteReducer: { note: { id: noteId } },
    } = getState();
    const users = await usersService.searchCollaboratorPretendersByEmail(
      noteId,
      email,
    );

    dispatch({
      type: SEARCH_USERS.SUCCESS,
      payload: {
        users,
      },
    });
  } catch ({ message }) {
    dispatch({
      type: SEARCH_USERS.FAIL,
      payload: {
        error: message,
      },
    });
  }
};

export const clearUsersSearch = () => ({
  type: SEARCH_FIELD_CLEAR,
});

export const addToCollaboratorsList = user => ({
  type: ADD_TO_COLLABORATORS_LIST,
  payload: {
    user,
  },
});

export const removeFromCollaboratorsList = userId => ({
  type: REMOVE_FROM_COLLABORATORS_LIST,
  payload: {
    userId,
  },
});

export const getCurrentUser = () => async (dispatch, getState) => {
  dispatch({ type: GET_CURRENT_USER.REQUEST });

  try {

    const user = await usersService.getCurrentUser();

    dispatch({
      type: GET_CURRENT_USER.SUCCESS,
      payload: {
        user,
      },
    });
  } catch ({ message }) {
    dispatch({
      type: GET_CURRENT_USER.FAIL,
      payload: {
        error: message,
      },
    });
  }
};
