import * as tokenService from '@services/token-service';
import {
  LOGIN_USER_FAIL,
  LOGIN_USER_SUCCESS,
  LOGOUT,
  REFRESH_TOKENS_FAIL,
  REFRESH_TOKENS_SUCCESS,
  REGISTER_USER_FAIL,
  REGISTER_USER_SUCCESS,
  USER_ALLOWED,
  USER_NOT_ALLOWED,
} from '@modules/authentication/action-names';

const initialState = {
  isLoggedIn: tokenService.tokenExists(),
  isAllowed: true,
};

function authReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOGIN_USER_SUCCESS:
    case REGISTER_USER_SUCCESS:
    case REFRESH_TOKENS_SUCCESS:
      return {
        ...state,
        isLoggedIn: tokenService.tokenExists(),
      };
    case LOGIN_USER_FAIL:
    case REGISTER_USER_FAIL:
    case REFRESH_TOKENS_FAIL:
      return {
        ...state,
        isLoggedIn: tokenService.tokenExists(),
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
      };
    case USER_NOT_ALLOWED:
      return {
        ...state,
        isAllowed: false,
      };
    case USER_ALLOWED:
      return {
        ...state,
        isAllowed: true,
      };
    default:
      return state;
  }
}

export default authReducer;
