import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import { Link } from 'react-router-dom';

import { makeUserAllowed } from '../actions';

const ForbiddenRoute = () => (
  <Fragment>
    <h1>Page not found or you kind of shouldn't be here</h1>
    <Link to={'/'}>Home</Link>
  </Fragment>
);

export default compose(
  connect(null, {
    makeUserAllowed,
  }),
  lifecycle({
    componentDidMount() {
      this.props.makeUserAllowed();
    },
  }),
)(ForbiddenRoute);
