import React from 'react';

import AuthLayout from '../components/auth-layout';
import RegisterForm from '../components/register-form';

const RegisterRoute = () => (
  <AuthLayout>
    <RegisterForm />
  </AuthLayout>
);

export default RegisterRoute;
