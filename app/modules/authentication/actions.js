import axios from 'axios';
import { getFormValues, SubmissionError } from 'redux-form';

import { closeSocketConnection } from '@modules/socket/actions';

import * as tokenService from '@services/token-service';
import * as authService from '@services/auth-service';

import { sanitizeError } from '@utils/errors';

import {
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LOGIN_USER_SUCCESS,
  LOGOUT,
  REGISTER_USER,
  REGISTER_USER_FAIL,
  REGISTER_USER_SUCCESS,
  USER_ALLOWED,
  USER_NOT_ALLOWED,
} from './action-names';

import { LOGIN_FORM_NAME, REGISTER_FORM_NAME } from './consts';

export const registerUser = () => async (dispatch, getState) => {
  const currentState = getState();
  const {
    email,
    password,
  } = getFormValues(REGISTER_FORM_NAME)(currentState);

  dispatch({ type: REGISTER_USER });

  try {
    const { data } = await axios.post('api/auth/register', {
      email,
      password,
    });
    const { token, refreshToken } = data;

    if (token && refreshToken) {
      tokenService.saveTokens({ token, refreshToken });

      dispatch({ type: REGISTER_USER_SUCCESS });
    }
  } catch ({ response: { data } }) {

    dispatch({
      type: REGISTER_USER_FAIL,
      payload: sanitizeError(data),
    });

    throw new SubmissionError(data);
  }
};

export const loginUser = () => async (dispatch, getState) => {
  const currentState = getState();
  const {
    email,
    password,
  } = getFormValues(LOGIN_FORM_NAME)(currentState);

  dispatch({ type: LOGIN_USER });

  try {
    const { data } = await axios.post('api/auth/login', {
      email,
      password,
    });
    const { token, refreshToken } = data;

    if (token && refreshToken) {
      tokenService.saveTokens({ token, refreshToken });

      dispatch({ type: LOGIN_USER_SUCCESS });
    }
  } catch ({ response: { data } }) {
    dispatch({
      type: LOGIN_USER_FAIL,
      payload: sanitizeError(data),
    });

    throw new SubmissionError(data);
  }
};

export const logout = () => dispatch => {
  authService.logout();
  dispatch({ type: LOGOUT });
  dispatch(closeSocketConnection());
};

export const makeUserNotAllowed = () => ({
  type: USER_NOT_ALLOWED,
});

export const makeUserAllowed = () => ({
  type: USER_ALLOWED,
});
