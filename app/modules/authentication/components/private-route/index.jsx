import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ isLoggedIn, isAllowed, ...props }) => {
  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  } else if (!isAllowed) {
    return <Redirect to="/forbidden" />;
  }

  return <Route {...props} />;
};

const mapStateToProps = ({ authReducer }) => ({
  isLoggedIn: authReducer.isLoggedIn,
  isAllowed: authReducer.isAllowed,
});

export default connect(mapStateToProps, null)(PrivateRoute);
