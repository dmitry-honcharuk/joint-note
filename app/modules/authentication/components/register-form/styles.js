export default theme => ({
  container: {
    width: '100%',
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  header: {
    ...theme.typography.headline,
    textAlign: 'center',
  },
});
