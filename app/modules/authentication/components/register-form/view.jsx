import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import { TextField } from '@modules/common';

import { required } from "@utils/validators";

const RegisterForm = ({ handleSubmit, registerUser, classes }) => (
  <Fragment>
    <h1 className={classes.header}>Sign Up</h1>
    <form
      className={classes.container}
      onSubmit={handleSubmit(registerUser)}>
      <TextField
        name="email"
        type="email"
        label="Email"
        validators={[required]}
      />
      <TextField
        name="password"
        type="password"
        label="Password"
        validators={[required]}
      />
      <div className={classes.footer}>
        <Button
          variant="contained"
          color="primary"
          type='submit'
        >
          Submit
        </Button>
        <Button
          component={Link}
          variant="text"
          to="/login"
        >
          Login
        </Button>
      </div>
    </form>
  </Fragment>
);

export default RegisterForm;
