import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';

import { registerUser } from '../../actions';
import RegisterFormView from './view';

import { REGISTER_FORM_NAME } from '../../consts';

import s from './styles';

export default compose(
  withStyles(s),
  connect(null, { registerUser }),
  reduxForm({
    form: REGISTER_FORM_NAME,
  }),
)(RegisterFormView);
