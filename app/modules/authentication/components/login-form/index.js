import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';

import { loginUser } from '../../actions';
import LoginFormView from './view';

import s from './styles';

import { LOGIN_FORM_NAME } from '../../consts';

export default compose(
  withStyles(s),
  connect(null, { loginUser }),
  reduxForm({
    form: LOGIN_FORM_NAME,
  }),
)(LoginFormView);
