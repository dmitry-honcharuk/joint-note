import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const AuthRoute = ({ isLoggedIn, redirectTo = '/', ...props }) => {
  if (isLoggedIn) {
    return <Redirect to={redirectTo} />;
  }

  return <Route {...props} />;
};

const mapStateToProps = ({ authReducer }) => ({
  isLoggedIn: authReducer.isLoggedIn,
});

export default connect(mapStateToProps, () => ({}))(AuthRoute);
