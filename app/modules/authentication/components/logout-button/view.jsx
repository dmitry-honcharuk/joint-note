import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string,
};

const defaultProps = {
  label: 'Logout',
};

const LogoutButton = ({ label, onClick }) => (
  <button type={'button'} onClick={onClick}>
    {label}
  </button>
);

LogoutButton.propTypes = propTypes;
LogoutButton.defaultProps = defaultProps;

export default LogoutButton;
