import { connect } from 'react-redux';

import LogoutButton from './view';

import { logout } from '../../actions';

const mapDispatchToProps = {
  onClick: logout,
};

export default connect(null, mapDispatchToProps)(LogoutButton);
