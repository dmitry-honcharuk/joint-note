import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flexBasis: '40%',
    maxWidth: 350,
  },
});

const AuthLayout = ({ children, classes }) => (
  <div className={classes.container}>
    <div className={classes.content}>
      {children}
    </div>
  </div>
);

export default withStyles(styles)(AuthLayout);
