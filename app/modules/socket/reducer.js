import { CONNECTION_CLOSED, CONNECTION_INITIALIZED } from './action-names';

const initialState = {
  isConnected: false,
};

function authReducer(state = initialState, action) {
  const { type } = action;

  switch (type) {
    case CONNECTION_INITIALIZED:
      return {
        isConnected: true,
      };
    case CONNECTION_CLOSED:
      return {
        isConnected: false,
      };
    default:
      return state;
  }
}

export default authReducer;
