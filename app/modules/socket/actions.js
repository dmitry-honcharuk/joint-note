import socketService from "@services/socket-service";

import { CONNECTION_CLOSED, CONNECTION_INITIALIZED, SOCKET_MESSAGE } from './action-names';

export const connectionInitialized = () => ({
  type: CONNECTION_INITIALIZED,
});

export const messageReceived = (payload) => ({
  type: SOCKET_MESSAGE,
  payload,
});

export const closeSocketConnection = () => {
  socketService.closeConnection();


  return {
    type: CONNECTION_CLOSED,
  };
};
