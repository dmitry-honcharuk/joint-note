import React from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

import { getCurrentUser } from '@modules/users/actions';
import { handleDropdownPick } from './actions';

import View from './view';

import s from './styles';

import { LOGOUT } from './consts';

const mapStateToProps = ({ currentUserReducer }, { classes }) => ({
  menuItems: [
    {
      id: LOGOUT,
      label: (
        <div>
          <div>Logout</div>
          <div className={classes.tip}>
            <span className={classes.email}>{currentUserReducer.email}</span>
          </div>
        </div>
      ),
    },
  ],
});

export default compose(
  withStyles(s),
  connect(mapStateToProps, {
    handleDropdownPick,
    getCurrentUser,
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCurrentUser();
    },
  }),
)(View);
