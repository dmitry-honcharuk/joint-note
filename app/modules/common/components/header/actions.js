import { logout } from '@modules/authentication/actions';

import { LOGOUT } from './consts';

export const handleDropdownPick = (id) => (dispatch) => {
  switch (id) {
    case LOGOUT :
      dispatch(logout());
  }
};
