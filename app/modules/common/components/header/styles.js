export default theme => ({
  container: {
    height: 70,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.secondary.light,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
  },
  icon: {
    margin: 0,
    padding: 0,
  },
  tip: {
    fontSize: '0.7rem',
  },
  email: {
    color: theme.palette.grey[500],
  },
  logoLink: {
    ...theme.typography.title,
    textTransform: 'uppercase',
    textDecoration: 'none',
    color: theme.palette.secondary.contrastText,
  },
});
