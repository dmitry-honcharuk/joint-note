import React from 'react';
import DehazeIcon from '@material-ui/icons/Dehaze';
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';

import { DropdownMenu } from '@modules/common';

const propTypes = {};
const defaultProps = {};

const Header = ({
  classes,
  handleDropdownPick,
  menuItems,
}) => (
  <div className={classes.container}>
    <div className={classes.logo}>
      <Link
        className={classes.logoLink}
        to={'/'}
      >Joint Note</Link>
    </div>
    <DropdownMenu
      id="header-dropdown"
      menuItems={menuItems}
      onItemPick={handleDropdownPick}
      trigger={(
        <IconButton>
          <DehazeIcon className={classes.icon} />
        </IconButton>
      )}
    />
  </div>
);

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;
