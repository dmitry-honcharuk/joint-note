import React, { Fragment } from 'react';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles'

import { Header } from '@modules/common';

import s from './styles';

const Layout = ({ children, classes }) => (
  <Fragment>
    <Header />
    <div className={classes.container}>
      {children}
    </div>
  </Fragment>
);

export default compose(
  withStyles(s),
)(Layout);
