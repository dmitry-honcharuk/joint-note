export default ({ typography, mixins }) => ({
  container: {
    ...typography.body1,
    ...mixins.gutters(),
  },
});
