import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const menuItemsProps = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.element,
    ]),
  }),
);

const propTypes = {
  id: PropTypes.string.isRequired,
  trigger: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
  menuItems: menuItemsProps,
  onItemPick: PropTypes.func.isRequired,
  closeOnItemPick: PropTypes.bool,
};
const defaultProps = {
  menuItems: [],
  closeOnItemPick: true,
};

class PopupMenu extends PureComponent {
  state = {
    anchorEl: null,
  };

  openDropdown = ({ target }) => {
    this.setState({
      anchorEl: target,
    });
  };

  closeDropdown = () => {
    this.setState({
      anchorEl: null,
    });
  };

  pickItem = (id) => {
    this.props.onItemPick(id);
    if (this.props.closeOnItemPick) {
      this.closeDropdown();
    }
  };

  render() {
    const {
      anchorEl,
    } = this.state;

    const {
      menuItems,
      id,
      trigger,
    } = this.props;

    return (
      <div>
        <div
          aria-owns={anchorEl ? id : null}
          aria-haspopup="true"
          onClick={this.openDropdown}
        >
          {trigger}
        </div>
        {menuItems.length && (
          <Menu
            id={id}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={this.closeDropdown}
          >
            {menuItems.map(({ id, label }) => (
              <MenuItem key={id} onClick={() => this.pickItem(id)}>{label || id}</MenuItem>
            ))}
          </Menu>
        )}
      </div>
    );
  }
}

PopupMenu.propTypes = propTypes;
PopupMenu.defaultProps = defaultProps;

export default PopupMenu;
