import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import cn from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const propTypes = {
  withErrorSpace: PropTypes.bool,
};
const defaultProps = {
  withErrorSpace: true,
};

const styles = theme => ({
  textField: {
    position: 'relative',
  },
  withErrorSpace: {
    marginBottom: theme.spacing.unit,
    paddingBottom: theme.typography.fontSize,
  },
  errorMessage: {
    color: theme.palette.error.light,
    position: 'absolute',
    right: 0,
  },
});

const renderField = ({ input, meta, classes, label, withErrorSpace }) => (
  <div className={cn(classes.textField, {
    [classes.withErrorSpace]: withErrorSpace,
  })}>
    <FormControl fullWidth>
      <InputLabel>{label}</InputLabel>
      <Input
        {...input}
      />
    </FormControl>
    {meta.touched && meta.error && (
      <FormHelperText className={classes.errorMessage} id="name-error-text">{meta.error}</FormHelperText>
    )}
  </div>
);

const TextField = ({ classes, validators, ...props }) => (
  <Field
    {...props}
    component={renderField}
    classes={classes}
    validate={validators}
  />
);

TextField.propTypes = propTypes;
TextField.defaultProps = defaultProps;

export default withStyles(styles)(TextField);
