import React from 'react';
import * as PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles'

import s from './styles';

const propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.any,
      key: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    }),
  ),
};

const defaultProps = {
  items: [],
};

const List = ({ items, classes }) => (
  !items.length
    ? null
    : (
      <ul className={classes.list}>
        {items.map(({ value, key }) => (
          <li key={key}>
            {value}
          </li>
        ))}
      </ul>
    )
);

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default compose(
  withStyles(s),
)(List);
