export const NOTE_DATE_FORMAT = 'hh:mm, MMMM Do, YYYY';
export const DELETE_NOTE = 'DELETE_NOTE';
export const DELETE_SHARED_NOTE = 'DELETE_SHARED_NOTE';
