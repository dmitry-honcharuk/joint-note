export function sanitizeError({ message, errorData }) {
  let payload = {};

  if (errorData && (errorData.errors || errorData.error)) {
    payload = errorData.errors || { error: errorData.error };
  } else {
    payload.internalError = message;
  }

  return payload;
}
