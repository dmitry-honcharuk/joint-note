import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

import { DashboardPage, NoteDetailsPage } from '@modules/notes';
import {
  ForbiddenPage,
  LoginPage,
  NotFoundPage,
  PrivateRoute,
  RegisterPage,
  UnauthorizedOnlyRoute
} from '@modules/authentication';

const Root = () => (
  <Fragment>
    <CssBaseline />
    <Switch>
      <PrivateRoute exact path="/" component={DashboardPage} />
      <UnauthorizedOnlyRoute path="/login" component={LoginPage} />
      <UnauthorizedOnlyRoute path="/register" component={RegisterPage} />
      <PrivateRoute path="/notes/:noteId" component={NoteDetailsPage} />
      <Route exact path="/forbidden" component={ForbiddenPage} />
      <Route component={NotFoundPage} />
    </Switch>
  </Fragment>
);

export default Root;
