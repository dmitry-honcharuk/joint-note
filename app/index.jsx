import React from 'react';
import ReactDom from 'react-dom';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import reducers from './reducers';
import { loggerMiddleware } from './middlewares/logger-middleware';
import { socketMiddleware } from './middlewares/socket-middleware';
import Root from './root';

const history = createHistory();
const theme = createMuiTheme({
  status: {
    danger: 'orange',
  },
});

const rMiddleware = routerMiddleware(history);
const rootEl = document.getElementById('app');
const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk, loggerMiddleware, socketMiddleware, rMiddleware),
);

ReactDom.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <MuiThemeProvider theme={theme}>
        <Root />
      </MuiThemeProvider>
    </ConnectedRouter>
  </Provider>,
  rootEl,
);
