import socketService from '../services/socket-service';

import { connectionInitialized, messageReceived } from '@modules/socket/actions';

export const socketMiddleware = ({ getState }) => next => action => {
  const {
    authReducer: { isLoggedIn },
  } = getState();
  const {
    socketReducer: { isConnected },
  } = getState();

  if (isLoggedIn && !isConnected) {
    socketService.initConnection();
    next(connectionInitialized());

    socketService.subscribe('action', data =>
      next(messageReceived(data)),
    );
  }

  next(action);
};
