import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux'

import authReducer from '@modules/authentication/reducer';

import socketReducer from '@modules/socket/reducer';

import notesReducer from '@modules/notes/reducers/notes-reducer';
import sharedNotesReducer from '@modules/notes/reducers/shared-notes-reducer';
import noteReducer from '@modules/notes/reducers/note-details-reducer';
import createNoteReducer from '@modules/notes/reducers/create-note-reducer';

import usersSearchReducer from '@modules/users/reducers/users-search-reducer';
import collaboratorPretenders from '@modules/users/reducers/collaborator-pretenders-reducer';
import currentUserReducer from '@modules/users/reducers/current-user-reducer';

export default combineReducers({
  authReducer,
  socketReducer,
  notesReducer,
  sharedNotesReducer,
  noteReducer,
  usersSearchReducer,
  collaboratorPretenders,
  currentUserReducer,
  createNoteReducer,
  form: formReducer,
  router: routerReducer
});
