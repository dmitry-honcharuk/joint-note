import * as http from '../http-service';

export const searchCollaboratorPretendersByEmail = async (noteId, email) => {
  const response = await http.get(`/api/notes/${noteId}/collaborators/search`, {
    params: {
      q: email,
    },
  });

  return response.data;
};

export const getCurrentUser = async () => {
  const response = await http.get(`/api/users/current`);

  return response.data;
};
