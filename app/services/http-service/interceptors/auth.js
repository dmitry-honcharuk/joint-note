import axios from 'axios';

import { refreshTokens } from "@services/auth-service";
import { getAuthHeader } from '@services/http-service';
import { clearTokens, saveTokens } from '@services/token-service';

export default {
  onSuccess: null,
  onError: async error => {
    const {
      config,
      response: { data },
    } = error;

    if (data && data.error && data.error === 'token expired') {
      try {
        const { data } = await refreshTokens();

        if (data && data.token && data.refreshToken) {
          saveTokens({ token: data.token, refreshToken: data.refreshToken });
        }

        config.headers = {
          ...config.headers,
          ...getAuthHeader(),
        };

        return axios.request(config);
      } catch (error) {
        console.error(error);

        clearTokens();
        location.reload();
      }
    }

    return Promise.reject(error);
  }
};
