import axios from 'axios';

import * as tokenService from '@services/token-service';

import authInterceptor from './interceptors/auth';

axios.interceptors.response.use(
  authInterceptor.onSuccess,
  authInterceptor.onError,
);

export function getBearer(token) {
  return `Bearer ${token}`;
}

export function getAuthHeader(token = tokenService.getToken()) {
  return {
    Authorization: getBearer(token),
  };
}

export function getHeaders(headers) {
  return {
    ...getAuthHeader(),
    ...headers,
  };
}

export function get(url, options = {}) {
  const headers = getHeaders(options.headers);

  return axios.get(url, {
    ...options,
    headers,
  });
}

export function del(url, options = {}) {
  const headers = getHeaders(options.headers);

  return axios.delete(url, {
    ...options,
    headers,
  });
}

export function post(url, data, options = {}) {
  const headers = getHeaders(options.headers);

  return axios.post(url, data, {
    ...options,
    headers,
  });
}
