import moment from 'moment';

import { NOTE_DATE_FORMAT } from '@consts/notes';
import * as http from '../http-service';

export const getNoteDisplayName = note =>
  note.name || moment(note.createdAt).format(NOTE_DATE_FORMAT) || '';

export const createNote = async (name, items) => {
  const response = await http.post('/api/notes', {
    name,
    items,
  });

  return response.data;
};

export const getPersonalNotes = async () => {
  const response = await http.get('/api/notes');

  return response.data;
};

export const getSharedNotes = async () => {
  const response = await http.get('/api/notes/shared');

  return response.data;
};

export const getNoteById = async id => {
  const response = await http.get(`/api/notes/${id}`);

  return response.data;
};

export const deleteNoteById = async id => {
  const response = await http.del(`/api/notes/${id}`);

  return response.data;
};

export const getCollaborators = async noteId => {
  const response = await http.get(`/api/notes/${noteId}/collaborators`);

  return response.data;
};

export const addCollaborators = async (noteId, collaborators) => {
  const response = await http.post(`/api/notes/${noteId}/collaborators`, {
    collaborators,
  });

  return response.data;
};

export const removeCollaborator = async (noteId, userId) => {
  const response = await http.del(
    `/api/notes/${noteId}/collaborators/${userId}`,
  );

  return response.data;
};

export const stopCollaborating = async noteId => {
  const response = await http.del(
    `/api/notes/shared/${noteId}`,
  );

  return response.data;
};
