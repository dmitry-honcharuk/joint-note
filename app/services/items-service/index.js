import * as http from '../http-service';

export const completeItem = async itemId => {
  const response = await http.get(`/api/items/${itemId}/complete`);

  return response.data;
};

export const undoItem = async itemId => {
  const response = await http.get(`/api/items/${itemId}/undo`);

  return response.data;
};

export const addItems = async (noteId, items) => {
  const response = await http.post(`/api/notes/${noteId}/items`, {
    items,
  });

  return response.data;
};
