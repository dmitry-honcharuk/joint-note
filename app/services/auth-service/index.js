import * as http from '../http-service';

import { clearTokens, getRefreshToken } from '../token-service';

export function refreshTokens() {
  const refreshToken = getRefreshToken();

  return http.get('/api/auth/refresh', {
    headers: {
      ...http.getAuthHeader(refreshToken),
    },
  });
}

export function logout() {
  clearTokens();
}
