const LOCAL_STORAGE_TOKEN_KEY = 'JOINT_NOTE_JWT_TOKEN';
const LOCAL_STORAGE_REFRESH_TOKEN_KEY = 'JOINT_NOTE_JWT_REFRESH_TOKEN';

export function getToken() {
  return localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY);
}

export function getRefreshToken() {
  return localStorage.getItem(LOCAL_STORAGE_REFRESH_TOKEN_KEY);
}

export function tokenExists() {
  return !!getToken();
}

export function saveToken(token) {
  if (token) {
    localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, token);
  }
}

export function saveRefreshToken(token) {
  if (token) {
    localStorage.setItem(LOCAL_STORAGE_REFRESH_TOKEN_KEY, token);
  }
}

export function saveTokens({ token, refreshToken }) {
  saveToken(token);
  saveRefreshToken(refreshToken);
}

export function clearTokens() {
  localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY);
  localStorage.removeItem(LOCAL_STORAGE_REFRESH_TOKEN_KEY);
}
