import io from 'socket.io-client';

function SocketServiceFactory() {
  let socket;

  return {
    initConnection: () => {
      socket = io();
    },
    subscribe: (event, handler) => {
      socket.on(event, handler);
    },
    emit: (event, data) => {
      socket.emit(event, data);
    },
    closeConnection: () => {
      socket.close();
    },
    validateSocket: token => {
      socket.emit('authenticate', {
        token,
      });
    },
  };
}

export default SocketServiceFactory();
