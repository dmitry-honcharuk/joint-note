const plan = require('flightplan');

const ROOT_FOLDER = '/var/www/';

// configuration
plan.target('stage', {
  host: '46.101.168.173',
  username: 'root',
  agent: process.env.SSH_AUTH_SOCK,
  branch: 'master',
  folder: 'jn.fictionaldevelopers.com',
});

plan.remote('init', remote => {
  const path = ROOT_FOLDER + remote.runtime.folder;

  return remote.with(`cd ${path}`, () => {
    fetchAndPull(remote);

    remote.log('[Install Dependencies]');
    remote.exec('npm install');

    remote.log('[Build Application]');
    remote.exec('npm run build:prod');

    remote.log('[Init Application]');
    remote.exec('npm run server:run');
  });
});

plan.remote(remote => {
  const path = ROOT_FOLDER + remote.runtime.folder;

  return remote.with(`cd ${path}`, () => {
    fetchAndPull(remote);

    remote.log('[Install Dependencies]');
    remote.exec('npm install');

    remote.log('[Build Application]');
    remote.exec('npm run build:prod');

    remote.log('[Reload Application]');
    remote.exec('pm2 reload all');
    return null;
  });
});

function fetchAndPull(remote) {
  const branch = plan.runtime.options.branch || remote.runtime.branch;

  remote.log('[Update Source]');
  remote.exec('git fetch --all');
  remote.exec(`git checkout ${branch}`);
  remote.exec(`git reset --hard origin/${branch}`);
}
